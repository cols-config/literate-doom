;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)
;; (package! ample-theme)
(package! dired-narrow)
;; (package! lsp-origami)
(package! modus-themes)
(package! org-projectile)
(package! persistent-scratch)

;; Installing smex causes swiper to show most used cmds first.
(package! smex)

;; (package! spacemacs-theme)
;; (package! vala-mode)
;;(package! which-key-posframe)
